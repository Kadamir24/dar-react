import { all, call } from "redux-saga/effects";
import { articlesSagas } from "./articles/articles.sagas";
import { categoriesSagas } from "./categories/categories.sagas";
import { articleSagas } from "./article/article.sagas";
import { authSagas } from "./auth/auth.sagas";

export default function* rootSaga() {
    yield all([
        call(categoriesSagas),
        call(articlesSagas),
        call(articleSagas),
        call(authSagas),
    ])
}