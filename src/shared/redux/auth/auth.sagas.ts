import { fetchProfileSuccess, fetchProfileError, fetchProfile, startLoginError, startLoginSuccess } from './auth.actions';
import { getProfile, login } from './../../api';
import { takeLatest, call, put, all } from 'redux-saga/effects';
import { AuthActionTypes, LoginAction } from './auth.types';


export function* fetchProfileAsync() {
    try {
        const res = yield call(getProfile);
        yield put(fetchProfileSuccess(res.data));
    } catch (e) {
        localStorage.removeItem('authToken');
        yield put(fetchProfileError(e.message));
    }
}

export function* fetchProfileSaga() {
    yield takeLatest(AuthActionTypes.FETCH_PROFILE, fetchProfileAsync);
}

export function* loginAsync(action: LoginAction) {
    try {
        const res = yield call(login, action.payload.username, action.payload.password);
        if (res.data.token) {
            localStorage.setItem('authToken', res.data.token)
            yield put(startLoginSuccess());
            yield put(fetchProfile());
            return;
        } 
        yield put(startLoginError('Unknown error'));
    } catch (e) {
        yield put(startLoginError(e.response?.data?.message));
    }
}

export function* loginSaga() {
    yield takeLatest(AuthActionTypes.LOGIN, loginAsync);
}

export function* authSagas() {
    yield all([
        call(loginSaga),
        call(fetchProfileSaga)
    ]);
}
