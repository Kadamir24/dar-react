import { createSelector } from 'reselect';
import { RootState } from '../root-reducers';

const selectAuthState = (state: RootState) => state.auth;

export const selectProfile = createSelector(
  [selectAuthState],
  (state) => state.profile
);

export const selectLoginError = createSelector(
  [selectAuthState],
  (state) => state.loginError
);

export const selectLoginLoading = createSelector(
  [selectAuthState],
  (state) => state.loginLoading
);

