
import { takeLatest, all, call, put } from 'redux-saga/effects';
import { getArticle, getArticles } from '../../api';
import { ArticlesActionTypes, fetchArticlesError, fetchArticlesSuccess } from './articles.actions';


export function* fetchArticlesAsync(action: any) {
    try {
        const res = yield call(getArticles, action.payload);
        console.log('CHETAM24', res.data);
        yield put(fetchArticlesSuccess(res.data))
    } catch(e) {
        yield put(fetchArticlesError(e))
    }
}

export function* fetchArticlesSaga() {
    yield takeLatest(ArticlesActionTypes.FETCH_ARTICLES, fetchArticlesAsync)
}

// export function* fetchArticleItemAsync(action: any) {
//     try {
//         const res = yield call(getArticle, action.payload);
//         console.log('CHETAMPOSTATEIKAM', res.data);
//         yield put(fetchArticlesSuccess(res.data))
//     } catch(e) {
//         yield put(fetchArticlesError(e))
//     }
// }

// export function* fetchArticleItemSaga() {
//     yield takeLatest(ArticlesActionTypes.FETCH_ARTICLES, fetchArticlesAsync)
// }

export function* articlesSagas() {
    yield all([
        call(fetchArticlesSaga)
    ]);
}