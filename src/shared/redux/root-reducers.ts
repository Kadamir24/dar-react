import { combineReducers } from "redux";
import { categoriesReducer } from "./categories/categories.reducer";
import { articlesReducer } from "./articles/articles.reducer";
import { articleReducer } from "./article/article.reducer";
import { authReducer } from "./auth/auth.reducer";

const rootReducers = combineReducers({
    category: categoriesReducer,
    articles: articlesReducer,
    article: articleReducer,
    auth: authReducer,
});

export type RootState = ReturnType<typeof rootReducers>

export default rootReducers;