import React, { useEffect, useReducer, useState } from 'react';
import './App.scss';
import Top from './components/top/Top';
import Counter from './components/top/header/counter/Counter';
import { Route, Switch } from 'react-router-dom';
import ArticlesPage from './pages/articles/ArticlesPage';
import ArticlePage from './pages/article/ArticlePage';
import AppContext, { ActionTypes, reducer } from './shared/app.context';
import LoginPage from './pages/login/LoginPage';
import { useDispatch } from 'react-redux';
import { fetchProfile } from './shared/redux/auth/auth.actions';

const App: React.FC = () => {

  // const initialState = JSON.parse(localStorage.getItem("user") || 'null');
  // const [state, dispatch] = useReducer(reducer, {
  //   profile: initialState,
  // })

  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem('authToken')
    if (token) {
      dispatch(fetchProfile());
    }
  }, [])


  return (
      <div className="App">
        <Top />
      <div>
        <Switch>
          <Route path="/counter" component={Counter}></Route>
          <Route exact path="/articles" component={ArticlesPage}/>
          <Route path="/articles/:categoryId" component={ArticlesPage}/>
          <Route path="/article/:articleId" component={ArticlePage} />
          <Route path="/login" component={LoginPage} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
