import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import CommentForm from '../../components/comment-form/CommentForm';
import { getArticle } from '../../shared/api';
import { fetchArticle } from '../../shared/redux/article/article.actions';
import { selectArticle } from '../../shared/redux/article/article.selectors';
import styles from './ArticlePage.module.scss';

const ArticlePage: React.FC = () => {

    const { articleId } = useParams<{articleId: string}>();

    // const [article, setArticle] = useState<Article | null>(null);
    const dispatch = useDispatch();
    const article = useSelector(selectArticle);
  

    useEffect(() => {
        if (articleId) {
            // getArticle(articleId)
            //     .then(res => setArticle(res.data))
            dispatch(fetchArticle(articleId));
        }
    }, [articleId, dispatch])

    return (
        <div className="ArticlePage">
            {
                article && (
                    <div className={styles.article_content}>
                        <div className={styles.article_content_title}>
                            {article.title} 
                        </div>
                        <div 
                            className={styles.article_content_annotation}
                            dangerouslySetInnerHTML={{__html: article.description}}>
                        </div>
                        <CommentForm />
                    </div>
                )
            }
        </div>
    )
}

export default ArticlePage;