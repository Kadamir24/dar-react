import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { ArticleList } from '../../components/main/Actual';
import { getArticles } from '../../shared/api';
import { fetchArticles, setArticles } from '../../shared/redux/articles/articles.actions';
import { selectArticles } from '../../shared/redux/articles/articles.selectors';
import { Article } from '../../shared/types';

const ArticlesPage: React.FC = () => {

    const { categoryId } = useParams<({ categoryId : string})>();

    // const [articles, setArticles] = useState<Article[]>([]);
    const dispatch = useDispatch();
    const articles = useSelector(selectArticles);
    useEffect(() => {
      // getArticles(categoryId)
      //   .then((articles) => {
      //     setArticles(articles)
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //   });
      
      // const fetchData = async () => {
      //   const res = await getArticles(categoryId);
      //   console.log('res', res.data)
      //   dispatch(setArticles(res.data));
      // }
      dispatch(fetchArticles(categoryId))
    }, [categoryId]);

    return (
        <div>
          <ArticleList articles={articles} />
        </div>
    )
}

export default ArticlesPage;