import React, { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { StyleSheetManager } from 'styled-components';
import Button from '../../components/button/Button';
import { getProfile, login } from '../../shared/api';
import AppContext, { ActionTypes } from '../../shared/app.context';
import { startLogin } from '../../shared/redux/auth/auth.actions';
import { selectLoginError, selectLoginLoading, selectProfile } from '../../shared/redux/auth/auth.selectors';
import styles from './LoginPage.module.scss';

const LoginPage: React.FC = () => {

    // const {dispatch} = useContext(AppContext);
    // const history = useHistory();
    const [form, setForm] = useState({username: '', password: ''});
    const history = useHistory();
    const dispatch = useDispatch();
    const profile = useSelector(selectProfile)
    const loginLoading = useSelector(selectLoginLoading);
    const loginError = useSelector(selectLoginError);

    const handleLogin = () => {
        if (form.password && form.username) {
            dispatch(startLogin(form.username, form.password));            
        }
    }

    useEffect(() => {
        if (profile) {
            history.replace('/');
        }
    }, [profile])


    // const handleLogin = () => {
    //     console.log(form);
    //     if (form.password && form.username) {
    //         login(form.username, form.password)
    //         .then(res => {
    //             if (res.data.token) {
    //                 localStorage.setItem('authToken', res.data.token)
    //             }
    //             return getProfile();
    //         })
    //         .then(res => {
    //             dispatch({type: ActionTypes.SET_PROFILE, payload: res.data});
    //             history.replace('/');
    //         })
    //     }
    // }

    return (
        <div className={styles.wrapper}>
            <h2>Log in</h2>
            <div>
                {loginError}
            </div>

            <div className={styles.controls}>
                <input 
                    className={styles.input} 
                    value={form.username} 
                    type="text" 
                    name="username"
                    onChange={(e) => {setForm(state => ({...state, username: e.target.value}))}} 
                    placeholder="Enter your username" />
                <input 
                    className={styles.input} value={form.password}
                    type="text"
                    name="password" 
                    onChange={(e) => {setForm(state => ({...state, password: e.target.value}))}}
                    placeholder="Enter your password" />
            </div>
            {/* <Button title="Login" variant="primary" onClick={handleLogin} /> */}
            {!loginLoading ? <Button title="Login" variant="primary" onClick={handleLogin} /> : <Button title="Loading" variant="ghost" />}
        </div>
    )
}

export default LoginPage;