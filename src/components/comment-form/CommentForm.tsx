import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import AppContext from '../../shared/app.context';
import { selectProfile } from '../../shared/redux/auth/auth.selectors';
import Button from '../button/Button';
import Input from '../input/Input';
import Profile from '../profile/Profile';
import styles from './Comment.module.scss';

const CommentForm: React.FC = () => {

    let [fields, setFields] = useState<{
        username: string;
        email: string;
        comment: string;
        usernameError: boolean;
        emailError: boolean;
    }>({
        username: '',
        email: '',
        comment: '',
        usernameError: true,
        emailError: true,
    });

    const onSubmit = () => {
        if (!isValidForm() || isEmpty()) {
            return;
        }
        console.log('submitted', fields)
        clear();
    }

    const isEmpty = () => {
        if (fields.username === '' || fields.email === '' || fields.comment === '') {
            return true;
        }
        return false;
    }

    const clear = () => {
        fieldChange('username', '');
        fieldChange('email', '');
        fieldChange('comment', '');
    }

    useEffect(() => {
        //TODO validate form fields
        console.log('FORM CHANGED', fields)
    }, [fields])

    const fieldChange = (fieldName: string, value: any) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: value
        }))
    }

    const fieldChangeValid = (fieldName: string) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: false
        }))
    }

    const fieldChangeInvalid = (fieldName: string) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: true
        }))
    }

    const isValidForm = () => {
        console.log('CHECK VALID', fields)
        if (fields.usernameError === true || fields.emailError === true) {
            return false;
        }
        return true;
    } 

    const profile = useSelector(selectProfile)

    // return (
    //     <AppContext.Consumer>
    //     {({state: {profile}}) => profile ? 
    //         (
    //         <div className={styles.wrapper}>
    //             <div className={styles.user}>
    //                 <Input type="Username" required fieldChangeValid={()=> fieldChangeValid('usernameError')} fieldChangeInvalid={()=> fieldChangeInvalid('usernameError')}
    //                 value={fields.username} onChange={(e: any)=> fieldChange('username', e.target.value)}/>
    //                 <Input type="Email" required fieldChangeValid={()=> fieldChangeValid('emailError')} fieldChangeInvalid={()=> fieldChangeInvalid('emailError')}
    //                 value={fields.email} onChange={(e: any) => fieldChange('email', e.target.value)} />
    //                   <Profile username={profile.username} avatar={profile.avatar} />
    //             </div>

    //             <div className={styles.comment}>
    //                 <div className={styles.formControl}>
    //                     <label>Comment</label>
    //                     <textarea name="comment" value={fields.comment} onChange={e => fieldChange('comment', e.target.value)}/>
    //                 </div>
    //             </div>

    //             <div className={styles.controls}>
    //                 <Button title="Submit" variant="primary" onClick={onSubmit} />
    //             </div>
    //         </div> 
    //         ) : <div className={styles.wrapper}>Чтобы оставить комментарий войдите в аккаунт</div>}
    //     </AppContext.Consumer>
    // )
    return (
        <AppContext.Consumer>
        {() => profile ? 
            (
            <div className={styles.wrapper}>
                <div className={styles.user}>
                    <Input type="Username" required fieldChangeValid={()=> fieldChangeValid('usernameError')} fieldChangeInvalid={()=> fieldChangeInvalid('usernameError')}
                    value={fields.username} onChange={(e: any)=> fieldChange('username', e.target.value)}/>
                    <Input type="Email" required fieldChangeValid={()=> fieldChangeValid('emailError')} fieldChangeInvalid={()=> fieldChangeInvalid('emailError')}
                    value={fields.email} onChange={(e: any) => fieldChange('email', e.target.value)} />
                      <Profile username={profile.username} avatar={profile.avatar} />
                </div>

                <div className={styles.comment}>
                    <div className={styles.formControl}>
                        <label>Comment</label>
                        <textarea name="comment" value={fields.comment} onChange={e => fieldChange('comment', e.target.value)}/>
                    </div>
                </div>

                <div className={styles.controls}>
                    <Button title="Submit" variant="primary" onClick={onSubmit} />
                </div>
            </div> 
            ) : <div className={styles.wrapper}>Чтобы оставить комментарий войдите в аккаунт</div>}
        </AppContext.Consumer>
    )
}

export default CommentForm;
