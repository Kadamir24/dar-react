import React from 'react';
import { Link } from 'react-router-dom';
import { getFormattedDayAndMonth } from '../../../shared/dateformat';
 import { Article } from '../../../shared/types';
import styles from './ArcticleItem.module.scss';


type Props = {
    article: Article;
}

const ArticleItem: React.FC<Props> = ({article}) => {
    const formatted_creation_date = getFormattedDayAndMonth(article.created_at);
    return (
        <div className={styles.article__wrapper}>
            <Link to={`/article/${article.id}`}>
                <div className = {styles.article__card}>
                    <div className={styles.article__card_left}>
                        <div className={styles.article__card_title}>{article.title}</div>
                        <div className={styles.article__card_annotation}>{article.annotation}</div>
                        <div className={styles.article__card_bottom}>
                        <span>{formatted_creation_date}</span>
                        </div>
                    </div>
                    <div className={styles.article__card_img}>
                        <img src={`https://dev-darmedia-uploads.s3.eu-west-1.amazonaws.com/${article.image}`} alt="image"/>
                    </div>
                </div>
            </Link>
        </div>
        
    );
  }
  
  export default ArticleItem;