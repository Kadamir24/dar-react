import React from 'react';
import ArticleItem from './articles/ArcticleItem';
import styles from './Actual.module.scss';
import { Article } from '../../shared/types';

type Props = {
  articles: Article[];
}

export const ArticleList: React.FC<Props> = ({articles}) => {

    return (
      <div className={styles.Actual__container}>
          <h4>Актуальное</h4>
          {articles.map(article => (
            <ArticleItem key={article.id} article={article} />
          ))}
      </div>
    );
}