import React, { useEffect, useState } from "react";
import styles from './../comment-form/Comment.module.scss';
import './Input.scss';


const emailValidator = (value: any) => (
  new RegExp(/\S+@\S+\.\S+/).test(value) ? "" : "Please enter a valid email."
);

const usernameValidator = (value: any) => (
  new RegExp(/^[a-zA-Z0-9]+$/).test(value) ? "" : "Please enter a valid username."
)

const Input = (props: any) => {

  let [fieldError, setFieldError] = useState<string | null>('');

  useEffect(() => {

    if (props.value === '' || !props.required) {
      setFieldError(null);
      props.fieldChangeValid();
      return;
    }

    if ((props.type === 'Email' && emailValidator(props.value) === '') ||
    (props.type === 'Username' && usernameValidator(props.value) === '')) {
      setFieldError(null);
      props.fieldChangeValid();
      return;
    }

    if (props.type === 'Username') {
      setFieldError(usernameValidator(props.value));
      props.fieldChangeInvalid();
      return;
    }

    setFieldError(emailValidator(props.value));
    props.fieldChangeInvalid();
  }, [props.type, props.value])



  return (
    <div className={styles.formControl}>
        <label>{props.type}</label>
        <input
        type="text"
        value={props.value}
        onChange={props.onChange}
        />
        {
          <span className="input__error">{fieldError}</span>
        }
    </div>
  );
};

export default Input;