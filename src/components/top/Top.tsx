import React, { useState } from 'react';
import AppContext from '../../shared/app.context';
import { Profile } from '../../shared/types';

import Header from './header/Header';
import Nav from './nav/Nav';

type Props = {

}

const topHeaderItems = ["О нас", "Обучение", "Сообщество", "Медиа", 'dar'];
const Top: React.FC<Props> = () => {
  const [topItems, setTopItems] = useState<string[]>(topHeaderItems);



  return (
    <div className="App">
       <Header items={topItems} />
       <Nav />
    </div>
  );
}

export default Top;