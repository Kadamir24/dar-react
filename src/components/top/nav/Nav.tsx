import React, { useEffect, useState } from 'react';
// import './Nav.scss';
import styles from './Nav.module.scss';
import nerv from './../../../logo.svg';
import { getCategories } from '../../../shared/api';
import { Category } from '../../../shared/types';
import { Link } from 'react-router-dom';
import { selectCategories } from '../../../shared/redux/categories/categories.selectors';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCategories, setCategories } from '../../../shared/redux/categories/categories.actions';

const Nav: React.FC = () => {
    const dispatch = useDispatch();
    // const [items, setItems] = useState<Category[]>([]);
    const categories = useSelector(selectCategories);
    console.log('CAT', categories);
    useEffect(() => {
        // const fetchData = async () => {
        //     const res = await getCategories();
        //     // setItems(res.data);
        //     dispatch(setCategories(res.data));
        // }
        // // fetchData();
        dispatch(fetchCategories());
    }, [])

    return (
        <div className={styles.Nav}>
            <ul className={styles.Nav__list}>
                <li><Link to="/articles">Все статьи</Link></li>
                {
                    categories.map((item) => <li key={item.id}><Link to={`/articles/${item.id}`}>{item.title}</Link></li>)
                }
                <li><Link to="/counter">Счётчик</Link></li>
                <li>
                    <a href="/#">
                        <img src={nerv} alt="logo" className={styles.Nav__logo}/>
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default Nav;
