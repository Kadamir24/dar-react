import React, { useContext } from 'react';
// import './Header.scss';
import styles from './Header.module.scss'
import nerv from './../../../assets/nerv.svg';
import Button from '../../button/Button';
import HeaderItems from './header-items/HeaderItems';
import AppContext, { ActionTypes } from '../../../shared/app.context';
import Profile from '../../profile/Profile';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectProfile } from '../../../shared/redux/auth/auth.selectors';
// import Counter from './counter/Counter';

type Props = {
    items: string[];
}

const Header: React.FC<Props> = ({items}) => {

    const {dispatch} = useContext(AppContext);
    // const {state: {profile}} = useContext(AppContext);

    const profile = useSelector(selectProfile)

    const history = useHistory();

    const goToLogin = () => {
        history.push('/login');
    }

    const exit = () => {
        dispatch({type: ActionTypes.RESET_PROFILE, payload: null});
        localStorage.removeItem('authToken')
        history.replace('/');
        window.location.reload();
    }

    return (
        <div className={styles.Header}>
            <header>
                <img src={nerv} alt="logo" className={styles.Header__logo}/>
                <ul className={styles.Header__list}>
                    <HeaderItems items={items}/>
                </ul>
                {
                    profile  ?
                    <div className={styles.Header__inProfile}>
                        <Profile username={profile.username} avatar={profile.avatar} />
                        <Button title={'Exit'} onClick={exit}/>
                    </div>  :
                        <div className={styles.Header__button}>
                            <Button title={'Login'} variant={'ghost'} onClick={goToLogin} />
                        </div>  
                }
            </header>
        </div>
    );
}

export default Header;
