import React, { useEffect, useState, useRef } from 'react';
import { checkLikes } from '../../../../shared/utils';
import Button from './../../../button/Button';
import styles from './Counter.module.scss';
const Counter: React.FC = () => {
    const [count, setCount] = useState<number>(0);
    const [message, setMessage] = useState<string>('');
    const [error, setError] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const initialLoad = useRef<boolean>(false);

    const minusCount = () => {
        setCount(prev => prev - 1);
    }

    const plusCount = () => {
        setCount(prev => prev + 1);
    }

    // useEffect(() => {
    //     if (!initialLoad.current) {
    //         initialLoad.current = true;
    //         return;
    //     }
    //     clearMessages();
    //     setLoading(true);
    //     checkLikes(count)
    //         .then(res => setMessage(res))
    //         .catch(err => setError(err))
    //         .finally(() => setLoading(false));
    // }, [count])
    useEffect(() => {
        if (!initialLoad.current) {
            initialLoad.current = true;
            return;
        }
        clearMessages();
        setLoading(true);
        const checkAsync = async () => {
            try {
                const res = await checkLikes(count)
                setMessage(res);
            } catch(err) {
                setError(err);
            }
            setLoading(false);
        }
        checkAsync();
    }, [count])

    const clearMessages = () => {
        setMessage('');
        setError('');
    }

    return (
        <div>
            <div className={styles.Counter}>
                <Button title={'-'} onClick={minusCount} variant={'secondary'} />
                <span>{count}</span>
                <Button title={'+'} onClick={plusCount} variant={'secondary'} />
                {message && <div>{message}</div>}
                {error ? <div>{error}</div> : ''}
                {loading && <div>Loading....</div>}
            </div>
        </div>
    )
}

export default Counter;