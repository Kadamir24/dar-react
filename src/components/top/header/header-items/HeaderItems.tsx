import React from 'react';

type Props = {
    items: string[];
}


const HeaderItems: React.FC<Props> = ({items}) => {
    return (
        <>
            {
                items.map((itemList, index) => {
                    return (
                        <li key={index}>{itemList}</li>
                    )
                })
            }
        </>
    )
}

export default HeaderItems;
